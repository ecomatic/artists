<?php
/**
 * The template for displaying all single posts and attachments.
 *
 * @package Hestia
 * @since Hestia 1.0
 */

get_header();

do_action( 'hestia_before_single_page_wrapper' );

?>
<div class="<?php echo hestia_layout(); ?>">
	<?php
	$class_to_add = '';
	if ( class_exists( 'WooCommerce', false ) && ! is_cart() ) {
		$class_to_add = 'blog-post-wrapper';
	}
	?>
	<div class="blog-post <?php esc_attr( $class_to_add ); ?>">
		<div class="container">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', 'page' );
				endwhile;
				else :
					get_template_part( 'template-parts/content', 'none' );
			endif;
				?>
		</div>
	</div>
	<?php get_footer(); ?>
	<?php
/**
 * The template for displaying all single posts and attachments.
 *
 * @package Hestia
 * @since Hestia 1.0
 */

get_header();

do_action( 'hestia_before_single_page_wrapper' );

?>
<div class="<?php echo hestia_layout(); ?>">
	<?php
	$class_to_add = '';
	if ( class_exists( 'WooCommerce', false ) && ! is_cart() ) {
		$class_to_add = 'blog-post-wrapper';
	}
	?>
	<div class="blog-post <?php esc_attr( $class_to_add ); ?>">
		<div class="container">
		<div class="row">

<main class="site-main" id="main">
	
	<?php
	$args = array( 'post_type' => 'my_offer', 'posts_per_page' => -1 );
	$loop = new WP_Query( $args );
	$id = 0;
	$i = 0;

	
	while ( $loop->have_posts() ) : $loop->the_post();

		the_title(sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
		'</a></h2>');

		$image = get_field('band_image');
		if( !empty( $image ) ): ?>
			<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>"/> <br>
		<?php endif; ?>
		<?php 
		$field = get_field_object('location');
			echo $field['label'];?>  <?php echo $field['value'] .'<br>'; 
			$field = get_field_object('band_size');
			echo $field['label'];?>  <?php  echo $field['value'] .'<br>'; 
				$field = get_field_object('length');
			echo $field['label']; ?>  <?php echo $field['value']. ' mins '.'<br>';
		?>
	<p class="mt-4">
		<a class="btn btn-primary" data-toggle="collapse" href="#Collapse<?php echo $id?>" role="button" aria-expanded="false" aria-controls="Collapse">My Offers</a>
	</p> 
<div class="row">
	<div class="col">
		<div class="collapse multi-collapse" id="Collapse<?php echo $id?>">
		<div class="card card-body">
			<?php
				$field = get_field_object('price');
				echo $field['label']; echo $field['value'].'<br>';
				// $field = get_field_object('price1');
				// echo $field['label']; echo $field['value'].'<br>';
			?>
		</div>
		</div>
	</div>
</div>			
	<?php
		echo '<div class="entry-content">';
		the_content();
		echo '</div>';

		$i++;
		$id++;
	endwhile;
	?>
</main><!-- #main -->

</div><!-- .row -->
		</div>
	</div>
	<?php get_footer(); ?>
