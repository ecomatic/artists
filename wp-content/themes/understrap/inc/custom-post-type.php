<?php
function cptui_register_my_cpts() {

/**
 * Post Type: My Offers.
 */

$labels = array(
    "name" => __( "My Offers", "understrap" ),
    "singular_name" => __( "My Offer", "understrap" ),
);

$args = array(
    "label" => __( "My Offers", "understrap" ),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array( "slug" => "my_offer", "with_front" => true ),
    "query_var" => true,
    "menu_icon" => "dashicons-star-filled",
    "supports" => array( "title", "editor", "thumbnail", "author" ),
    "taxonomies" => array( "category" ),
);

register_post_type( "my_offer", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );
