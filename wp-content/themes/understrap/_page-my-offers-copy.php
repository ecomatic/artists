<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
if (! is_user_logged_in()) {
	wp_redirect(esc_url(site_url('/')));
	exit;
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			

			<main class="site-main" id="main">

			<div class="container offering">
				<ul>
					<?php 
					$userOffers = new WP_Query([
						'post_type' => 'my_offer',
						'posts_per_page' => -1,
						'author' => get_current_user_id()->user_login
					]);

					while ($userOffers->have_posts()) {
						$userOffers->the_post(); ?>

<form action="/action_page.php">
  First name:<br>
  <input type="text" name="firstname" value="Mickey"><br>
  Last name:<br>
  <input type="text" name="lastname" value="Mouse"><br><br>
  <input type="submit" value="Submit">
</form>
							<li data-id="<?php the_ID(); ?>">
								<input class="my-offer-title-field" value="<?php echo esc_attr(get_the_title()); ?>">
								<h5>How many people?</h5>
								<input class="my-offer-title-field" value="<?php echo esc_attr(get_field('how_many_people')); ?>">
								<h5>Length</h5>
								<input class="my-offer-title-field" value="<?php echo esc_attr(get_field('length')); ?>">
								<h5>Price</h5>
								<input class="my-offer-title-field" value="<?php echo esc_attr(get_field('price')); ?>">		

								<span class="edit-blog"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span>
								<span class="delete-blog"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</span>	
							</li>
					
						<?php }
					?>		
				</ul>
			</div>

			</main><!-- #main -->

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
