<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
if (! is_user_logged_in()) {
	wp_redirect(esc_url(site_url('/')));
	exit;
}

get_header();


$container = get_theme_mod( 'understrap_container_type' );

?>
<?php


if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "new_post") {

    // Do some minor form validation to make sure there is content
    if (isset ($_POST['title'])) {
        $title =  $_POST['title'];
    } else {
        echo 'Please enter a title';
    }
    if (isset ($_POST['location'])) {
        $location =  $_POST['location'];
    } else {
        echo 'Please enter a your location';
    }
    if (isset ($_POST['image'])) {
        $image =  $_POST['image'];
    } else {
        echo 'Please enter a your band image';
	}
	if (!empty($_FILES)) {
		echo 'hi';
		$file = $_FILES['image'];
		$attachment_id = upload_user_file($file);
	 }
    if (isset ($_POST['band_size'])) {
        $bandSize =  $_POST['band_size'];
    } else {
        echo 'Please enter a your band size';
    }
    if (isset ($_POST['length'])) {
        $length =  $_POST['length'];
    } else {
        echo 'Please enter a length';
    }
    if (isset ($_POST['price'])) {
        $price =  $_POST['price'];
    } else {
        echo 'Please enter a price';
    }
    if (isset ($_POST['length1'])) {
        $length1 =  $_POST['length1'];
    } else {
        echo 'Please enter a length1';
    }
    if (isset ($_POST['price1'])) {
        $price1 =  $_POST['price1'];
    } else {
        echo 'Please enter a price1';
    }
    // if (isset ($_POST['description'])) {
    //     $description = $_POST['description'];
    // } else {
    //     echo 'Please enter the content';
    // }
    // $tags = $_POST['post_tags'];

	// Add the content of the form to $post as an array
	$new_post = array(
		'post_title'    => $title,
		'location'    => $location,
		'band_image' => $attachment_id,
		//'band_image' => $image,
        'post_category' => array(),  // Usable for custom taxonomies too
        'tags_input'    => array(),
		'post_status'   => 'publish', // Choose: publish, preview, future, draft, etc.
		'band_size' =>  $bandSize,
		'length' => $length,
		'price' => $price,
		'length1' => $length1,
		'price1' => $price1,
        'post_type' => 'my_offer'  //'post',page' or use a custom post type if you want to
	);
	//save the new post
	$pid = wp_insert_post($new_post); 
	add_post_meta( $pid, 'band_image',  $attachment_id, true );
	//add_post_meta( $pid, 'band_image',  $image, true );
	add_post_meta( $pid, 'location',  $location, true );
	add_post_meta( $pid, 'band_size',  $bandSize, true );
	add_post_meta($pid, 'length', $length, true );
	add_post_meta( $pid, 'price', $price, true );
	add_post_meta($pid, 'length1', $length1, true );
	add_post_meta( $pid, 'price1', $price1, true );
    //insert taxonomies
}

?>
<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">
			<main class="site-main" id="main">

			<div class="container offers-form">			

				<!-- New Post Form -->
				<div id="postbox">
					<form id="new_post" name="new_post" method="post" enctype="multipart/form-data" action="">

					<!-- post name -->
					<p><label for="title">Title</label><br />
					<input type="text" id="title" value="" tabindex="1" size="20" name="title" />
					</p>

					<p> <label for="image"> Select image to upload </label><br>
						<input type="file" name="image" id="image">
					</p>
					<p>
						<label for="location">Location</label><br>
						<input type="text" name="location" value="">
					</p>
					<!-- post Category -->
					<!-- <p><label for="Category">Category:</label><br />
					<p><?php //wp_dropdown_categories( 'show_option_none=Category&tab_index=4&taxonomy=category' ); ?></p> -->
					<p>
						<label for="band_size">How many people?</label><br>
						<input type="number" name="band_size" value="">
					</p>
					<p>
						<label for="length">How many minutes?</label><br>
						<input type="text" name="length" value="">
					</p>
					<p>
						<label for="price">Price SEK</label><br>
						<input type="number" name="price" value="">
					</p>
					<p>
						<label for="length1">Length</label><br>
						<input type="text" name="length1" value="">
					</p>
					<p>
						<label for="price1">Price</label><br>
						<input type="number" name="price1" value="">
					</p>

					<p align="right"><input type="submit" value="Submit" tabindex="6" id="submit" name="submit" /></p>

					<input type="hidden" name="action" value="new_post" />
					<?php wp_nonce_field( 'new-post' ); ?>
					</form>
				</div>
			</div>

			</main><!-- #main -->

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
