<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="archive-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<h1>Hello archive my offers I'm normal offers page</h1>
				<?php if ( have_posts() ) : ?>

<header class="page-header">
	<h1 class="page-title"><?php echo post_type_archive_title( '', false ); ?></h1>
</header><!-- .page-header -->

<?php /* Start the Loop */ ?>

<!-- This row hold 2 columns But you need to set class col-md-6 in file content-my_journeys.php inside loop-templates floder. -->
	<div class="row mt-4">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php

			/*
			* Include the Post-Format-specific template for the content.
			* If you want to override this in a child theme, then include a file
			* called content-___.php (where ___ is the Post Format name) and that will be used instead.
			*/
			//get_template_part( 'loop-templates/content-my_journeys', get_post_format() );
			?>
			
			
		<?php endwhile; ?>
	</div>
<?php else : ?>

<?php get_template_part( 'loop-templates/content', 'none' ); ?>

<?php endif; ?>
			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>

			

		</div> <!-- .row -->

	</div><!-- #content -->

	</div><!-- #archive-wrapper -->

<?php get_footer(); ?>
