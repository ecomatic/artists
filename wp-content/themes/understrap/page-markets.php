<?php
/**
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package understrap
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main row" id="main">

				<?php
				$args = array( 'post_type' => 'my_offer', 'posts_per_page' => -1 );
				$loop = new WP_Query( $args );
				$id = 0;

				while ( $loop->have_posts() ) : $loop->the_post();
				?>
				<div class="col-md-4 col-sm-6"> 
				<h2><?php the_title();?></h2>
				
				<div class="card" style="">
					<?php
					// image
					$image = get_field('band_image');
					if( !empty( $image ) ): ?>
					<div class="image-container" style=" overflow: hidden; height: 0; padding-top: 56.25%; background: url(<?php echo esc_url($image['url']); ?>) center; background-size: cover; ">
						<!-- <img src="<?php //echo esc_url($image['url']); ?>" alt="<?php //echo esc_attr($image['alt']); ?>"/> -->
					</div>

				<?php endif; ?>

				<div class="card-body">
				<?php
				$terms = wp_get_post_terms($post->ID, 'music_cat');
				$count = count($terms);
				if ( $count > 0 ) {?>
				
					<span class="dashicons dashicons-format-audio" style="display: inline;">
					<?php 
					 echo implode(
						', ', 
						array_map(
							function($term) { return $term->name; },
							$terms
						)
					);
					
				} ?>
				</span> <br>

					<!-- // Contents fields -->
					<span class="dashicons dashicons-location" style="display: inline;">
					<?php $field = get_field_object('location');
					echo $field['value'] .'<br>';
					?>
					</span>
					<?php
					$field = get_field_object('band_size');
					echo $field['label'];?>  <?php  echo $field['value'] .'<br>';
					$field = get_field_object('length');
					echo $field['label']; ?>  <?php echo $field['value']. ' mins '.'<br>';
				//	$field = get_field_object('spotify');
					//echo $field['label']; ?>  <?php //echo $field['value'].'<br>';

					?>
					<!-- BTN My offers -->
					<p class="mt-4">
						<a class="btn btn-primary" data-toggle="collapse" href="#Collapse<?php echo $id?>" role="button" aria-expanded="false" aria-controls="Collapse">My Offers</a>
					</p>

					<!-- contents inside BTN my offers -->
					<div class="row">
						<div class="col">
							<div class="collapse multi-collapse" id="Collapse<?php echo $id?>">
								<div class="card card-body">
									<?php
									$field = get_field_object('price');
									echo $field['label']; echo $field['value'].'<br>';?>
									<?php
									$price = get_field_object('price1');
									//echo var_dump($price);
									 if( $price['value']) { ?>
										<?php echo $price['label']; echo $price['value']; ?>
									<?php } ?>
									
								</div>
							</div>
						</div>
					</div>

					<p class="mt-4">
						<a class="btn btn-primary" data-toggle="collapse" href="#CollapseSpotify<?php echo $id?>" role="button" aria-expanded="false" aria-controls="Collapse">My Spotify</a>
					</p>

					<!-- contents inside BTN my Spotify -->
					<div class="spotify-wrapper">
						<div class="sportify-col">
							<div class="collapse multi-collapse" id="CollapseSpotify<?php echo $id?>">
								<div class="">
									<?php
									$field = get_field_object('spotify');
									echo $field['value'];
									?>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<?php
			//echo '<div class="entry-content">';
			//the_content();
			//echo '</div>';

			$id++;
			?>
			</div> <!-- .row inside while loop -->

		<?php endwhile;
		?>

	</main><!-- #main -->

</div><!-- .row -->

</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
